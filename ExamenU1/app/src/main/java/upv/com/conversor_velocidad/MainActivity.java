package upv.com.conversor_velocidad;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private Button convertir;
    private EditText cantidad;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        convertir = (Button) findViewById(R.id.button);
        convertir.setOnClickListener(new View.OnClickListener(){

            public void onClick(View arg0){
                cantidad = (EditText) findViewById(R.id.editText);
                resultado = (TextView) findViewById(R.id.textView1);
                try{
                    float ms = Float.parseFloat(cantidad.getText().toString());
                    float fh = (float) ((ms * 3600.0 * 1.0) / (1.0 * 1.0 * 0.3048));

                    resultado.setText(fh + " ft/h");
                }catch(Exception e){
                    resultado.setText("Ingrese solo numeros por favor");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
